/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: global variables, document ready initialization for click handlers.  Utility functions.
*/

// Global variables
var orgApiToken;
var orgId;
var agentId;
var eloquaBulkConnectionId;
var salesforceBulkConnectionId;
var marketoConnectionId;
var sugarCRMConnectionId;
var sampleCRMConnectionId;
var sampleMarketingConnectionId;
var eloquaBulkOAuthUrl;
var oAuthWaitingForStatusToBeRemovedCount = 0;
var oAuthWaitingForStatusToBecomePendingTest = 0;
var activeSolutionId;
var activeSolutionPrepareId;

// Tab variables (for IE support)
var tabTwo = "Configure"
var tabThree = "Monitor"

//Mapping related
var activeMap;
var sourceMapBlock;
var targetMapBlock;
var targetFields;
var getFieldsRetryCount;
var activeTargetField;
var cbv_ValueResults = []; //Array for value/result combinations for a ChooseByVal formula


/* Document Ready callback - wire up button click events after the DOM has loaded
   Also do html initialization which currently is just setting the parent orgid for the step 1 label.
*/
$(document).ready(function () {

    $('#get-child-orgs').on('click', getChildOrgs);
    $('#create-child-org').on('click', createChildOrg);
    $('#eloqua-bulk-save').on('click', createEloquaBulkConnection);
    $('#eloqua-bulk-update').on('click', updateEloquaBulkConnection);
    $('#salesforce-bulk-save').on('click', createSalesforceConnection);
    $('#marketo-save').on('click', createMarketoConnection);
    $('#sugarcrm-save').on('click', createSugarCRMConnection);
    $('#samplecrm-save').on('click', createSampleCRMConnection);
    $('#samplemarketing-save').on('click', createSampleMarketingConnection);

    $("#eloqua-bulk-test").on("click", function () {
        launchOAuthURLForConnection("EloquaBulk", eloquaBulkConnectionId);
    });

    $("#salesforce-bulk-test").on("click", function () {
        submitTestConnection("SalesforceBulk", salesforceBulkConnectionId);
    });
    $("#marketo-test").on("click", function () {
        submitTestConnection("Marketo", marketoConnectionId);
    });
    $("#sugarcrm-test").on("click", function () {
        submitTestConnection("SugarCRM", sugarCRMConnectionId);
    });
    $("#samplecrm-test").on("click", function () {
        submitTestConnection("SampleCRM", sampleCRMConnectionId);
    });
    $("#samplemarketing-test").on("click", function () {
        submitTestConnection("SampleMarketing", sampleMarketingConnectionId);
    });

    $('#clone-solution').on('click', cloneSolution);

    $('#prepare-solution').on('click', function () {
        prepareSolution('divPrepareSolution');
    });

    $('#check-solution-prepare-status').on('click', function () {
        getSolutionPrepareStatus('divPrepareSolution', 'divStartSolution');
    });

    $('#start-solution').on('click', function () {
        startSolution('divStartSolution');
    });

    $('#check-solution-start-status').on('click', function () {
        getSolutionStartStatus('divStartSolution');
    });

    //Tabs
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // Load the child orgs for the solution if the Mapping tab is selected
        var target = $(e.target).attr("href")
        if (target.indexOf(tabTwo) > 0)
            getChildOrgsForSelection('divSelectOrgForMapping');
        if (target.indexOf(tabThree) >0)
            getChildOrgsForSelection('divSelectOrgForMonitoring');

        /* Indlues not supported by IE
        if (target.includes('Configure'))
            getChildOrgsForSelection('divSelectOrgForMapping');
        else if (target.includes('Monitor'))
            getChildOrgsForSelection('divSelectOrgForMonitoring');
            */
    });

    // Element initialization
    $('#span-parent-orgid').text(PARENTORGID);
    $('#pApiUrl').text('API Url: ' + APIURL);

    // Mapping related handlers
    $('#save-map').on('click', saveMap);
    $('#validate-map').on('click', validateMap);
    $('#deleteAllMappings').on('click', deleteAllMappings);
    $('#btn-cbv-add').on('click', addCBVValueResult);
    $('#btn-save-choosebyvalue').on('click', saveChooseByVal);

    // Monitoring/Runtime handlers
    $('#start-solution-monitoring').on('click', function () {
        startSolution('divStartSolutionForMonitoring');
    });

    $('#check-solution-start-status-monitoring').on('click', function () {
        getSolutionStartStatus('divStartSolutionForMonitoring');
    });

    $('#solution-history').on('click', showSolutionHistory);
});




/******************************************** Utility functions ************************************************/


/* Function to loop through array and find a key match
 */
function findValueForConnectionPropertyKey(keyName, myArray) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].key === keyName) {
            return myArray[i].value;
        }
    }
}

/* Utility function to normalize the Scribe data type for display purposes
*/

function normalizeDataType(dataType, size) {
    var normalizedType;
    if (!dataType) {
        return "";
    }

    if (dataType.indexOf('System.') !== -1) {
        dataType = dataType.slice(dataType.indexOf('.') + 1, dataType.length);
    }
    dataType = dataType.toLowerCase();
    switch (dataType) {
        case "int16":
            normalizedType = "Integer16";
            break;
        case "int32":
            normalizedType = "Integer32";
            break;
        case "int64":
            normalizedType = "Integer64";
            break;
        case "uint16":
            normalizedType = "UnsignedInteger16";
            break;
        case "uint32":
            normalizedType = "UnsignedInteger32";
            break;
        case "uint64":
            normalizedType = "UnsignedInteger64";
            break;
        case "decimal":
            normalizedType = "Decimal";
            break;
        case "double":
            normalizedType = "Double64";
            break;
        case "single":
            normalizedType = "Double32";
            break;
        case "boolean":
            normalizedType = "Boolean";
            break;
        case "datetime":
            normalizedType = "DateTime";
            break;
        case "timespan":
            normalizedType = "TimeSpan";
            break;
        case "guid":
            normalizedType = "Guid";
            break;
        case "string":
            if (size === 0)
                normalizedType = "String";
            else
                normalizedType = "String(" + size + ")";
            break;
        case "byte[]":
            if (size === 0)
                normalizedType = "Byte Array";
            else
                normalizedType = "Byte Array(" + size + ")";
            break;
        case "byte":
            if (size === 0)
                normalizedType = "Byte";
            else
                normalizedType = "Byte(" + size + ")";
            break;
        case "char":
            normalizedType = "Char";
            break;
        default:
        case "object":
            normalizedType = "Object";
            break;
    }

    return normalizedType;
}


/* Extend Math to round a number to certain number of decimal places */
(function () {
    /**
     * Decimal adjustment of a number.
     *
     * @param {String}  type  The type of adjustment.
     * @param {Number}  value The number.
     * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
     * @returns {Number} The adjusted value.
     */
    function decimalAdjust(type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    // Decimal round
    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    // Decimal floor
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    // Decimal ceil
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})();

/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions related to encrypting and decrypting messages
*/

var ivSize = 16;
var keyLen = 32;
var iterations = 1000;
var apiSalt = "ac103458-fcb6-41d3-94r0-43d25b4f4ff4";

function EncryptMessage(textKey, secretMessage) {
    try {
        // Generate a random array of 16 bytes. This is the Initialization Vector (IV)
        // for the AES symmetric-key algorithm.
        var iv = forge.random.getBytes(ivSize);

        // Generate the 1000 iteration-derived key from the API Access Token key and the
        // Scribe Online API salt value using the Password-Based Key Derivation
        // Function 2 (PBKDF2) standard with Hash-based Message Authentication Code
        // calculated with Standard Encryption Algorithm 1 (HMAC-SHA1).
        var key = forge.pkcs5.pbkdf2(textKey, apiSalt, iterations, keyLen);

        // Encrypt the connection property value with the
        // AES algorithm using a key size of 256, a block size of 128,
        // and PKCS7 padding.
        var inputBuffer = forge.util.createBuffer(secretMessage);
        var cipher = forge.aes.startEncrypting(key, iv);
        cipher.update(inputBuffer);
        var status = cipher.finish();
        var encrypted = cipher.output.data;

        // Convert both the IV value and the encrypted Connection property values 
        // to Base 64-encoded text strings.
        // Append the Base 64-encrypted text string to the Base 64 IV text string.
        return forge.util.encode64(iv) + forge.util.encode64(encrypted);
    }
    catch (error) {
        alert("EncryptMessage Error:" + error.message);
    }
}

function DecryptMessage(textKey, encrypted) {
    try {
        // Separate and Base64 decode the encrypted message and iv
        var encryptedMessage = forge.util.decode64(encrypted.substr(ivSize + ivSize / 2));
        var iv = forge.util.decode64(encrypted.substr(0, ivSize + ivSize / 2));

        // Generate the 1000 iteration-derived key from the API Access Token key and the
        // Scribe Online API salt value using the Password-Based Key Derivation
        // Function 2 (PBKDF2) standard with Hash-based Message Authentication Code
        // calculated with Standard Encryption Algorithm 1 (HMAC-SHA1).
        var key = forge.pkcs5.pbkdf2(textKey, apiSalt, iterations, keyLen);

        // Encrypt the connection property value with the
        // AES algorithm using a key size of 256, a block size of 128,
        // and PKCS7 padding.
        var inputBuffer = forge.util.createBuffer(encryptedMessage);
        var cipher = forge.aes.startDecrypting(key, iv);
        cipher.update(inputBuffer);
        var status = cipher.finish();
        var secretMessage = cipher.output.data;

        return secretMessage;
    }
    catch (error) {
        alert("DecryptMessage Error:" + error.message);
    }
}
